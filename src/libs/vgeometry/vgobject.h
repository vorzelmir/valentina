/************************************************************************
 **
 **  @file   vgobject.h
 **  @author Roman Telezhynskyi <dismine(at)gmail.com>
 **  @date   27 12, 2013
 **
 **  @brief
 **  @copyright
 **  This source code is part of the Valentina project, a pattern making
 **  program, whose allow create and modeling patterns of clothing.
 **  Copyright (C) 2013-2015 Valentina project
 **  <https://gitlab.com/smart-pattern/valentina> All Rights Reserved.
 **
 **  Valentina is free software: you can redistribute it and/or modify
 **  it under the terms of the GNU General Public License as published by
 **  the Free Software Foundation, either version 3 of the License, or
 **  (at your option) any later version.
 **
 **  Valentina is distributed in the hope that it will be useful,
 **  but WITHOUT ANY WARRANTY; without even the implied warranty of
 **  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 **  GNU General Public License for more details.
 **
 **  You should have received a copy of the GNU General Public License
 **  along with Valentina.  If not, see <http://www.gnu.org/licenses/>.
 **
 *************************************************************************/

#ifndef VGOBJECT_H
#define VGOBJECT_H

#include <QSharedDataPointer>
#include <QString>
#include <QTypeInfo>
#include <QVector>
#include <QtGlobal>

#include "vgeometrydef.h"
#include "../vmisc/def.h"

class QLineF;
class QPoint;
class QPointF;
class QRectF;
class VGObjectData;
class QTransform;

/**
 * @brief The VGObject class keep information graphical objects.
 */
class VGObject
{
public:
    VGObject();
    explicit VGObject(const GOType &type, const quint32 &idObject = 0, const Draw &mode = Draw::Calculation);
    VGObject(const VGObject &obj);

    virtual ~VGObject();

    VGObject& operator= (const VGObject &obj);
#ifdef Q_COMPILER_RVALUE_REFS
    VGObject(const VGObject &&obj) Q_DECL_NOTHROW;
    VGObject &operator=(VGObject &&obj) Q_DECL_NOTHROW;
#endif

    quint32         getIdObject() const;
    void            setIdObject(const quint32 &value);

    virtual QString name() const;
    void            setName(const QString &name);

    Draw            getMode() const;
    void            setMode(const Draw &value);

    GOType          getType() const;
    void            setType(const GOType &type);

    quint32         id() const;
    virtual void    setId(const quint32 &id);

    virtual void    SetAlias(const QString &alias);
    QString         GetAlias() const;

    virtual void    SetAliasSuffix(const QString &aliasSuffix);
    QString         GetAliasSuffix() const;

    QString ObjectName() const;

    quint32         getIdTool() const;

    virtual QJsonObject ToJson() const;

    static QLineF  BuildLine(const QPointF &p1, const qreal& length, const qreal &angle);
    static QPointF BuildRay(const QPointF &firstPoint, const qreal &angle, const QRectF &scRect);
    static QLineF  BuildAxis(const QPointF &p, const qreal &angle, const QRectF &scRect);
    static QLineF  BuildAxis(const QPointF &p1, const QPointF &p2, const QRectF &scRect);

    static int     ContactPoints (const QPointF &p, const QPointF &center, qreal radius, QPointF &p1, QPointF &p2);
    static QPointF LineIntersectRect(const QRectF &rec, const QLineF &line);
    static int     IntersectionCircles(const QPointF &c1, double r1, const QPointF &c2, double r2, QPointF &p1,
                                       QPointF &p2);
    static qint32  LineIntersectCircle(const QPointF &center, qreal radius, const QLineF &line, QPointF &p1,
                                       QPointF &p2);
    static QPointF ClosestPoint(const QLineF &line, const QPointF &point);
    static QPointF addVector (const QPointF &p, const QPointF &p1, const QPointF &p2, qreal k);
    static void    LineCoefficients(const QLineF &line, qreal *a, qreal *b, qreal *c);
    static bool    IsPointOnLineSegment (const QPointF &t, const QPointF &p1, const QPointF &p2,
                                         qreal accuracy = accuracyPointOnLine);
    static bool    IsLineSegmentOnLineSegment (const QLineF &seg1, const QLineF &seg2,
                                               qreal accuracy = accuracyPointOnLine);
    static QPointF CorrectDistortion(const QPointF &t, const QPointF &p1, const QPointF &p2);
    static bool    IsPointOnLineviaPDP(const QPointF &t, const QPointF &p1, const QPointF &p2, 
                                       qreal accuracy = accuracyPointOnLine);
    static int GetLengthContour(const QVector<QPointF> &contour, const QVector<QPointF> &newPoints);
protected:
    static QTransform FlippingMatrix(const QLineF &axis);
private:
    QSharedDataPointer<VGObjectData> d;

    static int     PointInCircle (const QPointF &p, const QPointF &center, qreal radius);
};

Q_DECLARE_TYPEINFO(VGObject, Q_MOVABLE_TYPE);

#endif // VGOBJECT_H
